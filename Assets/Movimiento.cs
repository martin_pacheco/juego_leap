﻿using UnityEngine;
using System.Collections;
using Leap;

public class Movimiento : MonoBehaviour {
	Controller controller;
	public float velocidad;
	private float damageRobot=5.0f;
	private float damageBoton=1.0f;
	public int numDedo;
	public bool act = false;

	public float min=0f;
	public float max=2f;
	public float yRotation;
	public float angulo;

	// Use this for initialization
	void Start () {
		StartCoroutine (pausa());
		controller = new Controller();//Representa el controlador del Leap Motion, que nos da la información
	}
	
	// Update is called once per frame
	void Update () {
		Frame frame = controller.Frame ();//nos da la información del “Frame” actual
		GestureList gestos = frame.Gestures();// generamos una lista de gestos leidas en cada frame
		HandList hands = frame.Hands; // creamos el identificador de manos
		Hand firstHand = hands [0];//Leemos la mano derecha
		if (act == true) {
			for (int f = 0; f <hands[0].Fingers.Count; f++) {
				Finger digit = hands[0].Fingers [f];
				if (digit.IsExtended){
					numDedo = f;	     
				}
			}			
			//Rotacion horizontal
			yRotation += firstHand.PalmPosition.x * velocidad * Time.deltaTime;
			angulo=Mathf.Clamp (yRotation,min,max);
			this.transform.eulerAngles = Vector3.up * angulo;
		}
		//Para evitar retardos con los giros
		if (yRotation >= max)
			yRotation = max;
		if (yRotation <= min)
			yRotation = min;
		
		//se aplica un rayo del objeto, en direccion recta
		Ray rayo = new Ray(this.gameObject.transform.position,transform.forward); 
		RaycastHit hit;  

		if(numDedo==4){
			if(Physics.Raycast(rayo, out hit, 30.0f))
				hit.transform.SendMessage("DisminuirVida",damageRobot,SendMessageOptions.DontRequireReceiver);	
				hit.transform.SendMessage("botonazo",damageBoton,SendMessageOptions.DontRequireReceiver);	
		}
	}

	/*-----------pausa antes de iniciar el juego-------------*/
	public IEnumerator pausa(){
		yield return new WaitForSeconds(23.0f);
		act = true;
	}
		
}


