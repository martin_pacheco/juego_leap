﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Seguidor : MonoBehaviour {

	public Transform[] ruta;
	public float vel = 5.0f;
	public float dist = 1.0f;
	protected int pto=0;
	public int contadorRobot=2;
	private Animator anima;
	public bool act = false;
	private float vida=10.0f;
	public Image[] imagenrobots;
	public Image perder;
	public bool sigueJuego=true;
	public Animator[] Bandas;
	public SpriteRenderer tubo;
	public AudioSource sonidito;

	void Awake(){
		anima = GetComponent<Animator> ();
	}

	// Use this for initialization
	void Start () {
		StartCoroutine (pausa());
	}
	
	// Update is called once per frame
	void Update () {
		//tomar la distancia entre robot un punto
		float dir=Vector3.Distance(ruta[pto].position,transform.position);
		//camina hasta que GeneradorBalas.cs active act como true
		anima.SetBool ("caminar", act);

		if (act == true) {
			//interpolacion en cada punto declarado
			//Inicio de la animacion de las bandas
			for (int i = 0; i < 3; i++) {
				Bandas [i].enabled = true;	
			}
			transform.position = Vector3.Lerp (transform.position, ruta [pto].position, Time.deltaTime * vel);
		}
		//si la distancia entre robot y punto es menor a la indicada se avanza al sig punto
		if (dir <= dist) {
			pto++;
		}
		//si el robot llega a su punto 9 se ejecuta metodo vidasRobot
		if (pto == 11) {
			vidasRobot (1);
		}
		//si se llega al ultimo punto vuelve al inicial
		if (pto >= ruta.Length) {
			pto = 0;
		}


	}

	/*-----------disminuye la vida del robot, ejecuta la animacion y se destruye el objeto-------------*/
	public void DisminuirVida(float damage){
		vida -= damage;
		if(vida<=0f){
			//objeto activa animacion si es atacado y disminuido vida
			GeneradorBalas gb= GameObject.Find("Lanzador").GetComponent<GeneradorBalas>();
			//si el juego continua se aumenta el numero de muertes de robots, y tienen que estar en animacion de caminar
			if (sigueJuego == true && act==true) {
				gb.muertes++;
			}
			act = false;
			anima.SetBool("muerto",true);
			sonidito.Play ();
			DestroyObject (this.gameObject, 1.5f);
		}
	}

	/*-----------disminuye las imagenes de vida del robot-------------*/
	public void vidasRobot(int valor){
		//si el valor es -1, enviado desde el script de boton.cs, aumentara la imagen de vida
		if (valor < 0 && contadorRobot < 2 && sigueJuego == true) {
			contadorRobot -= valor;
			imagenrobots [contadorRobot].enabled = true;
		}
		//si valor es 1, disminuira la imagen de vida
	   if (contadorRobot >= 0 && valor > 0) {
			contadorRobot -= valor;
			imagenrobots [contadorRobot + 1].enabled = false;
			print ("robot fuera :" + (contadorRobot + 1));
		}
		//si queda una imagen de vida, el tubo cambiara a color rojo alertando
		if (contadorRobot == 0) {	
			tubo.color=new Color(1.0f,0f,0f);	
		}
		//al disminuir todas las imagenes, se desactiva el conteo de robots muertos
		if (contadorRobot < 0) {
			perder.enabled = true;
			GameObject[] robots = GameObject.FindGameObjectsWithTag ("robot");
			foreach(GameObject bloque in robots){
				bloque.GetComponent<Seguidor> ().sigueJuego = false;
			}
		}
		
	}

	/*-----------pausa antes de iniciar el juego-------------*/
	public IEnumerator pausa(){
		yield return new WaitForSeconds(20.0f);
		act = true;
	}

}
