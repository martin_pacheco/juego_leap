﻿using UnityEngine;
using System.Collections;
using Leap;
using UnityEngine.UI;

public class GeneradorBalas : MonoBehaviour {
	public int num;
	public Rigidbody proyectil;
	public float VelocidadBala = 12.0f;
	public GameObject robotGen;
	public AudioSource AS;
	public AudioClip[] AC;

	public Text texto;
	public UnityEngine.UI.Image ganar;
	public float muertes=0f;
	int i=0;
	int contador=0;
	public Transform[] lugar;

	// Use this for initialization
	void Start () {
		StartCoroutine (balillas ());
		StartCoroutine (generadorRobots());
	}

	// Update is called once per frame
	void Update () {
		//texto del score
		texto.text=" "+muertes.ToString("0");
		//para poder ganar se necesitan un numero de muertes y que no haya terminado el conteo de vidas
		GameObject[] robots = GameObject.FindGameObjectsWithTag ("robot");
		foreach(GameObject bloque in robots){
			//se obtine el valor actual del contador
			contador=bloque.GetComponent<Seguidor> ().contadorRobot;
		}
		//si el score es 30 y aun no se pierde, condicion de ganar
		if (muertes >= 30f && contador>=0) {
			ganar.enabled = true;
			foreach(GameObject bloque in robots){
				//al ganar se inicializa contadorRobot a un valor alto, para no perder enseguida
				bloque.GetComponent<Seguidor> ().contadorRobot=10;
				bloque.GetComponent<Seguidor> ().sigueJuego = false;
			}
		}
		//asignacion del audioclip en el audiosource
		AS.clip = AC[i]; 

	}

	/*-----------generador de las balas-------------*/
	public IEnumerator balillas(){
		while(true){
			//se obtiene el numero de dedo de otro script y es asignado a variable
			num = GameObject.Find ("Arma").GetComponent<Movimiento> ().numDedo;
			if(num == 4){
				//se crea una bala que es disparada por el arma
				Rigidbody bala = (Rigidbody)Instantiate (proyectil,transform.position, transform.rotation);
				bala.velocity = transform.TransformDirection (new Vector3 (0.0f, 0.0f, VelocidadBala));
				AS.Play ();
				DestroyObject (bala.gameObject, 2);
			}
			//tiempo en el que sera generada la bala	
			yield return new WaitForSeconds(0.5f);
			//variable i altera los cuatro audioclips
			i++;
			if (i >= 4)
				i = 0;
		}
	}

	/*-----------generador de todos los robots-------------*/
	public IEnumerator generadorRobots(){
		while(true){
			//busqueda de objeto robot, se creara instancia del mismo
			GameObject robot = GameObject.FindGameObjectWithTag ("robot");
			//al existir robots en la escena, la instancia se hace en el punto 1
			if (robot != null) {
				Instantiate (robot, lugar[0].position, Quaternion.identity);
			//si desaparecen todos los robots, se llama al prefab y se instancian en los tres distintos puntos
			} else if (robot == null) {
				for (int i = 0; i < 9; i++) {
					//Generador de puntos 
					robotGen.GetComponent<Seguidor> ().ruta [i] = GameObject.Find ("Punto"+(i+1)).transform;
				}
				Instantiate (robotGen, lugar[Random.Range(0, 2)].position, Quaternion.identity);
			}
			yield return new WaitForSeconds(3.0f);
		}
	}

}
