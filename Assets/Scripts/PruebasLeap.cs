﻿using UnityEngine;
using System.Collections;
using Leap;

public class PruebasLeap : MonoBehaviour {
	
	Controller controlador;
	public float Velocidad;
	// Use this for initialization
	void Start () {
		controlador = new Controller();//Representa el controlador del Leap Motion, que nos da la información 
		Frame frame = controlador.Frame ();
	}
	
	// Update is called once per frame
	void Update () {
		Frame frame = controlador.Frame ();//Siempre leer el controlador
		Hand Mano=frame.Hands.Frontmost;
		Vector CentrodePalma = Mano.PalmPosition;
		if (CentrodePalma.y > 90 && CentrodePalma.y < 240) {
			print ("Entre"+CentrodePalma.y);
			//this.transform.Translate (CentrodePalma.x * Velocidad * Time.deltaTime,0.0f, 0.0f);
			this.transform.position=new Vector3(Mano.PalmPosition.x,Mano.PalmPosition.y-90.0f,2.366234f);
		}
	}
}
