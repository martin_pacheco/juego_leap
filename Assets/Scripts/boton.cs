﻿using UnityEngine;
using System.Collections;

public class boton : MonoBehaviour {
	private float toque = 0f;
	private Vector3 valor = Vector3.back;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		//busqueda de objetos vacios, seran los limites del boton
		GameObject ruta = GameObject.Find ("Vacio");
		GameObject ruta2 = GameObject.Find ("Vacio2");

		//se obtiene la distancia entre el boton y los objetos vacios
		float dir = Vector3.Distance(ruta.transform.position,transform.position);
		float dir2 = Vector3.Distance (ruta2.transform.position, transform.position);

	

		//movimiento del boton	
		transform.Translate(valor * Time.deltaTime*2f);
		if (dir <= 0.5f) {
			//ir hacia abajo
			valor = Vector3.forward;
		}
		if (valor == Vector3.forward && dir2 <= 0.5f) {
			//ir hacia arriba
			valor = Vector3.back;
		}
	}

	/*---------metodo activado cada que toque la bala al boton----------------*/
	public void botonazo(float dato){
		toque += dato;
		print ("toques boton "+toque);
		if (toque == 15f) {
			//si se a tocado el boton n veces, se manda un valor al metodo para aumentar vidas
			GameObject[] gb = GameObject.FindGameObjectsWithTag ("robot");
			foreach(GameObject bloque in gb){
				bloque.GetComponent<Seguidor> ().vidasRobot (-1);
			}
			toque = 0f;
		}
	}
}
